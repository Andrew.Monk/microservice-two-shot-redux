import { useEffect, useState } from 'react';

function HatList(props) {
	const [hats, setHats] = useState([])

const fetchData = async () => {
	const url = "http://localhost:8090/api/hats/";
	console.log(url);

	const response = await fetch(url);
	console.log(response);

	if (response.ok) {
		const data = await response.json();
		console.log(data);
		setHats(data.hats)
	} else {
		console.log('its not working');
	}
}

useEffect(() => {
	fetchData();
}, []);

return (
	<div className="px-4 py-5 my-5 text-center">
		<h1 className="display-5 fw-bold">Check Out My Hats</h1>
		<div className="col-lg-6 mx-auto">
			<p className="lead mb-4">
				I've got some cool hats to show you
			</p>
		</div>
		<table className="table table-striped mx-auto">
			<thead>
				<tr key='info'>
					<th key='style'>Style</th>
					<th key='fabric'>Fabric</th>
					<th key='color'>Color</th>
					<th key='location'>Location</th>
					<th key='picture_url'>Picture</th>
				</tr>
				</thead>
			<tbody>
				{hats.map(hat => (
					<tr key={hat.id}>
						<td key='style'>{hat.style_name}</td>
						<td key='fabric'>{hat.fabric}</td>
						<td key='color'>{hat.color}</td>
						<td key='location'>{hat.location.closet_name}</td>
						<td key='picture_url'>
						<img width='75' src={hat.picture_url} />
						</td>
						</tr>
				))}
			</tbody>
		</table>
	</div>
);
}

export default HatList;
