import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import HatList from './HatList';
import HatCreateForm from './HatCreateForm';
import Nav from './Nav';
import ShoeForm from './ShoeForm';
import CreateShoe from './CreateShoe'


function App() {
  // if (props.hats === undefined) {
  //   return <h1>Hold my beer</h1>
  // }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="create-shoe" element={<CreateShoe />} />
          <Route path="shoes" element={<ShoeForm />} />
          <Route path='hats' element={<HatList />} />
          <Route path='create' element={<HatCreateForm />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
