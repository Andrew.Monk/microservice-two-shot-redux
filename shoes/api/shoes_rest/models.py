from django.db import models
from django.urls import reverse


class BinVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=50)


class Shoes(models.Model):
    name = models.CharField(max_length=50)
    style = models.CharField(max_length=50)
    manufacturer = models.CharField(max_length=20)
    size = models.SmallIntegerField()
    color = models.CharField(max_length=50)
    picture_url = models.URLField(null=True)

    bin = models.ForeignKey(
        BinVO,
        related_name="shoes",
        on_delete=models.CASCADE,
        null=True,
    )

    def __str__(self):
        return self.name

    def get_api_url(self):
        return reverse("api_bin", kwargs={"pk": self.pk})
