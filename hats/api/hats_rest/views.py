from .models import Hat, LocationVO
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder


class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "closet_name",
        "import_href",
        # 'id',
        # "shelf_number",
        # "section_number",
    ]


class HatListEncoder(ModelEncoder):
    model = Hat
    properties = [
        'style_name',
        'fabric',
        'color',
        'location',
        'picture_url',
    ]
    encoders = {
        'location': LocationVODetailEncoder(),
    }


class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        'fabric',
        'picture_url',
        'style_name',
        'color',
        'location',
    ]
    encoders = {
        'location': LocationVODetailEncoder(),
    }


@require_http_methods(['GET', 'POST'])
def list_hats(request, location_vo_id=None):
    if request.method == "GET":
        if location_vo_id is not None:
            hats = Hat.objects.filter(location=location_vo_id)
            return JsonResponse(
                {"message": location_vo_id}
            )
        else:
            hats = Hat.objects.all()
        return JsonResponse(
            {'hats': hats},
            encoder=HatListEncoder
        )
    else:
        content = json.loads(request.body)
        print(content)
        # if 'location' not in content:
        #     return JsonResponse(
        #         {'message': content}, status=400
        #     )

        try:
            location_href = content['location']
            print(location_href)
            location = LocationVO.objects.get(import_href=location_href)
            # location = LocationVO.objects.get(location_href=import_href)
            content['location'] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {'message': "invalid location"},
                status=400,
            )

        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False
        )


@require_http_methods(['DELETE', 'GET', 'PUT'])
def hat_detail(request, pk):
    if request.method == 'GET':
        hat = Hat.objects.get(id=pk)
        return JsonResponse(
            {'hat': hat}, encoder=HatDetailEncoder
        )
    elif request.method == 'DELETE':
        count, _ = Hat.objects.filter(id=pk).delete()
        return JsonResponse(
            {'deleted': count > 0}
        )
    else:
        content = json.loads(request.body)
        try:
            if 'location' in content:
                location = Hat.objects.get(location=content['closet_name'])
                content['location'] = location
        except Hat.DoesNotExist:
            return JsonResponse(
                {'message': 'invalid location'}, status=400
            )
        Hat.objects.filter(id=pk).update(**content)
        hat = Hat.objects.get(id=pk)
        return JsonResponse(
            hat, encoder=HatDetailEncoder, safe=False
        )
